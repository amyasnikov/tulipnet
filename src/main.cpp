
#include <experimental/filesystem>
#include <iostream>
#include <iomanip>
#include <cstddef>
#include <fstream>
#include <vector>
#include <set>
#include "logger.h"
#include "../3rdparty/nlohmann/json.hpp"

#include <cryptopp/sha3.h>

namespace fs = std::experimental::filesystem;



template <typename tvector_t = std::vector<char>>
struct transport_t
{
  tvector_t& vector;

  transport_t(tvector_t& vector) : vector(vector) { }

  size_t size() { return vector.size(); }

  void write(const void* data, size_t size)
  {
    vector.insert(vector.end(), (const char*)data, size + (const char*)data);
  }

  size_t read(void* data, size_t s)
  {
    if (s > size())
      return 0;

    memcpy(data, vector.data(), s);
    vector.erase(vector.begin(), vector.begin() + s); // TODO
    return s;
  }
};

struct node_t;

struct config_t
{
  static inline std::string objects_name = "objects";
  static inline std::string config_name  = "config";

  std::string address;
  std::string host_id;
  std::string path_repo;
  size_t storage_max_size;
  // std::string hash_alg;
  // std::string bootstrap;

  static inline size_t storage_gc_watermark = 80;      // 80%
  static inline size_t storage_gc_period = 60;         // 1min
  static inline size_t object_size_max = 256 * 1024;   // 256KB
  static inline bool   use_hidden_files = false;
  static inline size_t version = 1;

  void init(node_t&)
  {
    address = "127.0.0.1:8440";
    host_id = "";
    path_repo = "/home/alexander/.tulipnet";
    storage_max_size = 1 * 1024 * 1024 * 1024; // 1GB
  }

  bool load(node_t&)
  {
    LOGGER_BASE;

    if (!fs::exists(path_repo))
    {
      LOG_BASE("ERROR: directory '%s' is not exists", path_repo.c_str());
      return false;
    }

    fs::path path_config = path_repo / fs::path(config_name);

    std::ifstream file(path_config.c_str());

    nlohmann::json config;

    file >> config;

    if (auto it = config.find("address"); it != config.end())
      address = *it;

    if (auto it = config.find("host_id"); it != config.end())
      host_id = *it;

    if (auto it = config.find("path_repo"); it != config.end())
      path_repo = *it;

    if (auto it = config.find("storage_max_size"); it != config.end())
      storage_max_size = *it;

    return true;
  }

  bool save(node_t&)
  {
    LOGGER_BASE;

    if (!fs::exists(path_repo) && !fs::create_directory(path_repo))
    {
      LOG_BASE("ERROR: can not create directory '%s'", path_repo.c_str());
      return false;
    }

    if (fs::path path_objects = path_repo / fs::path(objects_name);
        !fs::exists(path_objects) && !fs::create_directory(path_objects))
    {
      LOG_BASE("ERROR: can not create directory '%s'", path_objects.c_str());
      return false;
    }

    fs::path path_config = path_repo / fs::path(config_name);

    std::ofstream file(path_config.c_str());

    nlohmann::json config =
    {
      {"address", address},
      {"host_id", host_id},
      {"path_repo", path_repo},
      {"storage_max_size", storage_max_size},
    };

    file << std::setw(2) << config << std::endl;

    return true;
  }
};

struct address_t // TODO
{
  std::string address;
};

struct hash_t // SHA3_256
{
  using alg_t = CryptoPP::SHA3_256;
  static const inline size_t digest_size = alg_t::DIGESTSIZE;

  unsigned char hash[digest_size];

  void calc(const void* data, size_t size)
  {
    alg_t().CalculateDigest(hash, (unsigned char *) data, size);
  }

  bool operator==(const hash_t& h) const
  {
    return !memcmp(hash, h.hash, digest_size);
  }

  bool operator<(const hash_t& h) const
  {
    return memcmp(hash, h.hash, digest_size) < 0;
  }

  std::string str() const
  {
    std::stringstream stream;
    for (auto it = std::begin(hash); it != std::end(hash); ++it)
      stream << std::setfill('0') << std::setw(2) << std::hex << (int) *it;
    return stream.str();
  }
};

struct object_blob_t
{
  hash_t hash;
  std::vector<char> data;

  bool is_valid() const
  {
    hash_t h;
    h.calc(data.data(), data.size());
    return h == hash;
  }
};

struct connection_t
{
  // context
};

struct job_t // request?
{
  time_t begin;
  time_t end;
  size_t id;
  size_t type;
};

struct object_mng_t
{
  static inline char flag_dir  = 'D';
  static inline char flag_file = 'F';

  const config_t* config;
  std::set<hash_t> objects;
  std::set<hash_t> pinned;

  void init(node_t& node);

  void process(node_t&) { }

  hash_t add_objects(const std::string& fname)
  {
    LOGGER_BASE;

    hash_t hash;

    if (fs::is_directory(fname))
    {
      LOG_BASE("INFO: directory: '%s'", fname.c_str());
      hash = add_objects_dir(fname);
    }
    else if (fs::is_regular_file(fname))
    {
      LOG_BASE("INFO: file: '%s'", fname.c_str());
      hash = add_objects_file(fname);
    }
    else
    {
      LOG_BASE("WARN: unknown file: '%s'", fname.c_str());
    }

    return hash;
  }

  hash_t add_objects_dir(const std::string& fname)
  {
    LOGGER_BASE;

    std::multimap<hash_t, std::string> hashes;

    for (auto& entry: fs::directory_iterator(fname))
    {
      std::string fname = entry.path().filename();
      if (!config_t::use_hidden_files && !fname.empty() && fname.front() == '.')
        continue;

      hashes.insert({add_objects(entry.path().native()), fname});
    }

    object_blob_t object;
    transport_t transport(object.data);
    transport.write(&flag_dir, sizeof(flag_dir));
    for (const auto& [hash, fname] : hashes)
    {
      transport.write(hash.hash, sizeof(hash.hash));
      unsigned char s = fname.size();
      transport.write(&s, sizeof(s));
      transport.write(fname.c_str(), s);
    }
    object.hash.calc(object.data.data(), object.data.size());
    save_object(object);
    LOG_BASE("INFO: add dir hash: '%s'", object.hash.str().c_str());

    return object.hash;
  }

  hash_t add_objects_file(const std::string& fname)
  {
    LOGGER_BASE;

    size_t size = config_t::object_size_max;
    std::ifstream file(fname.c_str());
    object_blob_t object;
    std::vector<hash_t> hashes;

    while (true)
    {
      object.data.resize(size);
      size_t s = file.readsome(object.data.data(), size);
      object.data.resize(s);

      object.hash.calc(object.data.data(), object.data.size());
      LOG_BASE("INFO: hash: '%s'", object.hash.str().c_str());

      save_object(object);
      hashes.push_back(object.hash);

      if (!s) break;
    }

    transport_t transport(object.data);
    transport.write(&flag_file, sizeof(flag_file));
    for (const auto& hash : hashes)
    {
      transport.write(hash.hash, sizeof(hash.hash));
    }
    object.hash.calc(object.data.data(), object.data.size());
    save_object(object);
    LOG_BASE("INFO: add file hash: '%s'", object.hash.str().c_str());

    return object.hash;
  }

  void save_object(const object_blob_t& object)
  {
    LOGGER_BASE;
    LOG_BASE("INFO: save hash: '%s'", object.hash.str().c_str());

    fs::path path = config->path_repo / fs::path(config->objects_name) / fs::path(object.hash.str());

    std::ofstream file(path.c_str());
    file.write(object.data.data(), object.data.size());
  }

  bool get_objects(const std::string& fname, hash_t hash)
  {
    LOGGER_BASE;
    LOG_BASE("INFO: get hash: '%s'", hash.str().c_str());

    object_blob_t object;
    object.hash = hash;

    get_object(object);

    transport_t transport(object.data);

    if (transport.size() < 1)
    {
      LOG_BASE("ERROR: object '%s' is invalid", object.hash.str().c_str());
      return false;
    }

    char flag;
    if (transport.read(&flag, sizeof(flag)) != sizeof(flag))
    {
      LOG_BASE("ERROR: can not read flag");
      return false;
    }

    if (flag == flag_dir)
    {
      if (!fs::exists(fname) && !fs::create_directory(fname))
      {
        LOG_BASE("ERROR: can not create directory '%s'", fname.c_str());
        return false;
      }

      hash_t hash;
      unsigned char s;
      std::string fname_inner;
      while (transport.size())
      {
        transport.read(hash.hash, sizeof(hash.hash));
        transport.read(&s, sizeof(s));
        fname_inner.resize(s);
        transport.read(fname_inner.data(), s);
        fname_inner = (fs::path(fname) / fs::path(fname_inner)).native();

        if (!get_objects(fname_inner, hash))
        {
          LOG_BASE("ERROR: can not get object '%s'", hash.str().c_str());
          return false;
        }
      }
    }
    else if (flag == flag_file)
    {
      std::ofstream file(fname.c_str());
      object_blob_t object;
      while (transport.size())
      {
        transport.read(object.hash.hash, sizeof(object.hash.hash));
        if (!get_object(object))
        {
          LOG_BASE("ERROR: can not get object '%s'", hash.str().c_str());
          return false;
        }
        file.write(object.data.data(), object.data.size());
      }
    }
    else
    {
      LOG_BASE("ERROR: object '%s' had invalid flag", object.hash.str().c_str());
      return false;
    }

    return true;
  }

  bool get_object(object_blob_t& object)
  {
    LOGGER_BASE;

    fs::path path = config->path_repo / fs::path(config->objects_name) / fs::path(object.hash.str());

    if (!fs::exists(path))
    {
      LOG_BASE("ERROR: object '%s' is not exists", object.hash.str().c_str());
      return false;
    }

    std::ifstream file(path.c_str());
    object.data.assign(std::istreambuf_iterator<char>{file}, {});

    bool res = object.is_valid();

    if (!res)
    {
      LOG_BASE("ERROR: object '%s' is not valid", object.hash.str().c_str());
    }

    return res;
  }

};

struct net_mng_t
{
  void init(node_t&) { }
  void process(node_t&) { }
};

struct node_t
{
  config_t config;
  net_mng_t net_mng;
  object_mng_t object_mng;

  void init()
  {
    if (!config.load(*this))
    {
      config.init(*this);
      if (!config.save(*this))
      {
        LOG_BASE("ERROR: can not save config");
      }
    }

    object_mng.init(*this);
    net_mng.init(*this);
  }

  void process()
  {
    object_mng.process(*this);
    net_mng.process(*this);
  }
};



void object_mng_t::init(node_t& node)
{
  config = &node.config;
}



int main(int argc, char* argv[])
{
  LOGGER_BASE;

  if (argc != 2)
  {
    std::cerr << "Usage: " << argv[0] << " <working directory>" << std::endl;
    return 1;
  }

  node_t node;
  node.config.path_repo = argv[1];
  node.init();

  // while (true)
  {
    try
    {
      node.process();
    }
    catch (std::exception& e)
    { }
  }

  {
    hash_t hash = node.object_mng.add_objects("/mnt/media/tmp/tulipnet_test/dir2");
    node.object_mng.get_objects("/mnt/media/tmp/tulipnet_test/dir2o", hash);
  }

  {
    // hash_t hash = node.object_mng.add_objects("/mnt/media/tmp/tulipnet_test/file3.png");
    // node.object_mng.get_objects("/mnt/media/tmp/tulipnet_test/file3o.png", hash);
  }

  return 0;
}

