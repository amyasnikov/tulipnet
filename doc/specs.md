


# Общее описание

Peer addressing
/ip4/5.6.7.8/tcp/5678/ip4/1.2.3.4/sctp/1234/

HostId = hash(publicKey)

object format:

{
  type: tree
  name: "video"
  hash: <hash>
  links: [<hash1>]
  data: []
  size: 4031
}
{
  type: blob
  name: "video1.mkv"
  hash: <hash1>
  links: []
  data: ["MKV..."]
  size: 4031
}



links:
* http://tutorials.jenkov.com/p2p/index.html
* https://pracai.com/ipfs-introduction-to-interplanetary-file-system/



